# Adakerja-Nodejs-Coding-Challenge

The task is to build a Facebook messenger bot in Node.js

# Get Started

-   Clone the repository using git clone https://gitlab.com/odetayodeji/adakerja-nodejs-coding-challenge
-   Run `npm i` or `npm install` to install all app dependencies
-   Make a copy of the .env.example file and rename to .env
-   PORT=3000 or any port of your choice
-   DATABASE_URI= <YOUR_DB_URI>
-   PAGE_ACCESS_TOKEN=<YOUR_PAGE_TOKEN>

```

-   Start the app at the root directory using
    -   `npm run start`

## To run the app via docker

```

docker-compose up

```

### Facebook Messenger Setup
- Please follow this tutorial: https://developers.facebook.com/docs/messenger-platform/getting-started/quick-start


# Demo

The base endpoint is an health check endpoint that returns a success response.

---

-   ### API Security implemented in this task

1. Add a rate limit for requests of 20 requests per 10 minutes using `express-rate-limit`.
2. Secure HTTP headers using `helmet`.
   3 . Use `CORS` to indicate origins that are permitted to load resources.

Note - For a more robust application, more security implementations will be done

---

# Choice of design pattern

I used a layered architecture that contains layers like repository, services, controllers and routes.
The reason for this is to follow good practices in software development by separating concerns and making the code reusable and readable.

## API

| Parameter   |  Description          |
| ----------- | -------------- ------ |
| Base Url    | /                     |
| Http Method | GET                   |
| Path        | /api/messages         |
| Http Method | GET                   |
| Path        | /api/messages/:id     |
| Http Method |   POST                |
| Path        | /api/webhook          |
| Http Method |   GET                 |
| Path        | /api/webhook          |

# Project Structure

```

├── docker-compose.yml
├── Dockerfile
├── jest.config.js
├── nodemon.json
├── package.json
├── package-lock.json
├── README.md
├── src
│   ├── common
│   │   └── common.routes.config.ts
│   ├── controllers
│   │   ├── index.ts
│   │   ├── message.controller.ts
│   │   ├── user.controller.ts
│   │   └── webhook.controller.ts
│   ├── helpers
│   │   └── api.response.ts
│   ├── index.ts
│   ├── middleware
│   │   ├── index.ts
│   │   └── messages.middleware.ts
│   ├── models
│   │   ├── index.ts
│   │   ├── message.model.ts
│   │   └── user.model.ts
│   ├── repositories
│   │   ├── index.ts
│   │   ├── message.repositories.ts
│   │   └── user.repositories.ts
│   ├── routes
│   │   ├── index.ts
│   │   ├── messages.routes.config.ts
│   │   └── webhook.route.ts
│   ├── services
│   │   ├── index.ts
│   │   ├── message.services.ts
│   │   └── user.services.ts
│   └── tests
│   ├── message.test.ts
│   └── user.test.ts
└── tsconfig.json

```

# Libraries/Frameworks Used

-   Nodejs - JavaScript runtime environment for building backend applications.
-   Express - Express. js is a free and open-source web application framework for Node. js.
    It is used for designing and building web applications quickly and easily.
    -   Mocha - For running tests
        -MongoDB - for managing database

# Testing

-   To run the tests, simply run this command `npm test`

Thank you 👍
```
