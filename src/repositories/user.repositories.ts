import { userModel as User } from '../models/index';
import { IUser } from '../models/user.model';

class UserRepository {
    async createUser(payload: IUser): Promise<IUser> {
        const user = await User.create(payload);
        return user;
    }

    async getUserById(senderId: number): Promise<IUser> {
        const user = await User.findById(senderId);

        if (!user) {
            throw new Error('not found');
        }
        return user;
    }
    async getUsers(): Promise<IUser[]> {
        return User.find({});
    }

    async updateUserById(senderId: number, payload: any): Promise<IUser> {
        return User.findByIdAndUpdate(senderId, payload, { upsert: true });
    }

    async deleteUserById(senderId: number): Promise<boolean> {
        const deletedUser = await User.findByIdAndDelete(senderId);
        return !!deletedUser;
    }
}

const userRepository = new UserRepository();
export default userRepository;
