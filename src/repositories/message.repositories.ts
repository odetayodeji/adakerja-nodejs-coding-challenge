import { messageModel as Message } from '../models/index';
import { IMessage } from '../models/message.model';

class MessageRepository {
    async createMessage(payload: IMessage): Promise<IMessage> {
        const message = await Message.create(payload);
        return message;
    }

    async getMessageById(mid: string): Promise<IMessage> {
        const message = await Message.findById(mid);

        if (!message) {
            // @todo throw custom error
            throw new Error('not found');
        }
        return message;
    }

    async getMessages(): Promise<IMessage[]> {
        return Message.find({});
    }

    async updateMessageById(mid: string, payload: IMessage): Promise<IMessage> {
        return Message.findByIdAndUpdate(mid, payload, { upsert: true });
    }

    async deleteMessageById(mid: string): Promise<boolean> {
        const deletedMessage = await Message.findByIdAndDelete(mid);
        return !!deletedMessage;
    }
}

const messageRepository = new MessageRepository();
export default messageRepository;
