import messageRepository from './message.repositories';
import userRepository from './user.repositories';

export { messageRepository, userRepository };
