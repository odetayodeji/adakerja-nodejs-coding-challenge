import express from 'express';

import { messageServices } from '../services/index';

class MessageMiddleware {
    async validateRequiredBodyFields(req: express.Request, res: express.Response, next: express.NextFunction) {
        if (req.body && req.body.senderId && req.body.timestamp && req.body.mid && req.body.text) {
            next();
        } else {
            res.status(400).send({
                error: `Missing required fields question and possible answers`
            });
        }
    }

    async validateMessageExists(req: express.Request, res: express.Response, next: express.NextFunction) {
        const message = await messageServices.getMessage(req.params.id);
        if (message) {
            next();
        } else {
            res.status(404).send({
                error: `Message with id ${req.params.id} not found`
            });
        }
    }
}

export default new MessageMiddleware();
