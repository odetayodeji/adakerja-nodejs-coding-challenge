import { IMessage } from '../models/message.model';
import messageRepository from '../repositories/message.repositories';

class MessageServices {
    async createNewMessage(payload: IMessage): Promise<IMessage> {
        return await messageRepository.createMessage(payload);
    }

    async updateMessage(mid: string, payload: IMessage): Promise<IMessage> {
        return await messageRepository.updateMessageById(mid, payload);
    }
    async getMessage(mid: string): Promise<IMessage> {
        return await messageRepository.getMessageById(mid);
    }
    async getAllMessages(): Promise<IMessage[]> {
        return await messageRepository.getMessages();
    }
    async deleteMessage(mid: string): Promise<boolean> {
        return await messageRepository.deleteMessageById(mid);
    }
}

const messageServices = new MessageServices();
export default messageServices;
