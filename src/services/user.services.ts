import { IUser } from '../models/user.model';
import userRepository from '../repositories/user.repositories';

class UserServices {
    async createNewUser(payload: IUser): Promise<IUser> {
        return await userRepository.createUser(payload);
    }

    async updateUser(senderId: number, payload: any): Promise<IUser> {
        return await userRepository.updateUserById(senderId, payload);
    }
    async getUser(senderId: number): Promise<IUser> {
        return await userRepository.getUserById(senderId);
    }

    async getAllUsers(): Promise<IUser[]> {
        return await userRepository.getUsers();
    }

    async deleteMessage(senderId: number): Promise<boolean> {
        return await userRepository.deleteUserById(senderId);
    }
}

const userServices = new UserServices();
export default userServices;
