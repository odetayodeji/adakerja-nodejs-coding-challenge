import mongoose, { ConnectOptions } from 'mongoose';
import { messageServices as MessageService } from '../services/index';
const TEST_DB_URI = process.env.TEST_DB_URI;

beforeAll(async () => {
    await mongoose
        .connect(TEST_DB_URI, {
            useNewUrlParser: true,
            useCreateIndex: true,
            useUnifiedTopology: true
        } as ConnectOptions)
        .then(() => {
            console.log('database connection established');
        })
        .catch((err) => {
            console.log(err);
        });
});

afterEach(async () => {
    await removeAllCollections();
});

afterAll(async () => {
    await mongoose.connection.close();
});

test('it should create new message', async () => {
    const message = await MessageService.createNewMessage({
        senderId: 10003456,
        timestamp: 1616987364458,
        mid: 'm_283091456',
        text: 'some text'
    });

    expect(message).toMatchObject({
        sender_id: 10003456,
        timestamp: 1616987364458,
        mid: 'm_28309456',
        text: 'some text'
    });
});

test('it should get message by id', async () => {
    await MessageService.createNewMessage({
        senderId: 1234,
        timestamp: 1610987334458,
        mid: 'm_1111',
        text: 'some text'
    });

    const message = await MessageService.getMessage('m_1111');

    expect(message).toMatchObject({
        sender_id: 1234,
        timestamp: 1610987334458,
        mid: 'm_1111',
        text: 'some text'
    });
});

test('it should get all messages', async () => {
    await MessageService.createNewMessage({
        senderId: 1234,
        timestamp: 1610987334458,
        mid: 'm_1234',
        text: 'some text'
    });

    const message = await MessageService.getAllMessages();

    expect(message[0]).toMatchObject({
        sender_id: 1234,
        timestamp: 1610987334458,
        mid: 'm_1234',
        text: 'some text'
    });
});

test('it should delete message by id', async () => {
    await MessageService.createNewMessage({
        senderId: 1234,
        timestamp: 1610987334458,
        mid: 'm_1234',
        text: 'some text'
    });

    const result = await MessageService.deleteMessage('m_1234');

    expect(result).toMatchObject({
        n: 1,
        ok: 1,
        deletedCount: 1
    });
});

async function removeAllCollections() {
    const collections = Object.keys(mongoose.connection.collections);
    for (const collectionName of collections) {
        const collection = mongoose.connection.collections[collectionName];
        await collection.deleteMany({});
    }
}
