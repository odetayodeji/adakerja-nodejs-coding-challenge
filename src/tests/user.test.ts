import mongoose, { ConnectOptions } from 'mongoose';
import { userServices as UserService } from '../services/index';

const TEST_DB_URI = process.env.TEST_DB_URI;

beforeAll(async () => {
    await mongoose
        .connect(TEST_DB_URI, {
            useNewUrlParser: true,
            useCreateIndex: true,
            useUnifiedTopology: true
        } as ConnectOptions)
        .then(() => {
            console.log('database connection established');
        })
        .catch((err) => {
            console.log(err);
        });
});

afterEach(async () => {
    await removeAllCollections();
});

afterAll(async () => {
    await mongoose.connection.close();
});

test('it should create a new user', async () => {
    const user = await UserService.createNewUser({
        senderId: 23131312,
        firstName: 'deji',
        birthDate: '2000-07-21'
    });

    expect(user).toMatchObject({
        senderId: 23131312,
        firstName: 'deji',
        birthDate: '2000-07-21'
    });
});

test('create new user with only senderId', async () => {
    const user = await UserService.createNewUser({
        senderId: 23131312
    });

    expect(user).toMatchObject({
        senderId: 23131312
    });
});

test('get user by id', async () => {
    await UserService.createNewUser({
        senderId: 23131312,
        firstName: 'deji',
        birthDate: '2000-07-21'
    });

    const user = await UserService.getUser(23131312);

    expect(user).toMatchObject({
        senderId: 23131312,
        firstName: 'deji',
        birthDate: '2000-07-21'
    });
});

test('get users', async () => {
    await UserService.createNewUser({
        senderId: 23131312,
        firstName: 'deji',
        birthDate: '2000-07-21'
    });

    const user = await UserService.getAllUsers();

    expect(user[0]).toMatchObject({
        senderId: 23131312,
        firstName: 'deji',
        birthDate: '2000-07-21'
    });
});

test('update user by id', async () => {
    await UserService.createNewUser({
        senderId: 23131312
    });

    await UserService.updateUser(23131312, {
        firstName: 'deji',
        birthDate: '2000-07-21'
    });

    const user = await UserService.getUser(23131312);

    expect(user).toMatchObject({
        senderId: 23131312,
        firstName: 'deji',
        birth_date: '2000-07-21'
    });
});

async function removeAllCollections() {
    const collections = Object.keys(mongoose.connection.collections);
    for (const collectionName of collections) {
        const collection = mongoose.connection.collections[collectionName];
        await collection.deleteMany({});
    }
}
