import mongoose, { Schema, Document } from 'mongoose';

export interface IUser {
    senderId: number;
    firstName?: string;
    birthDate?: string;
}

const UserSchema: Schema = new Schema({
    senderId: {
        type: Number,
        required: true
    },
    firstName: {
        type: String,
        required: false
    },
    birthDate: { type: String, required: false }
});

export default mongoose.model<IUser>('User', UserSchema);
