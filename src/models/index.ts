import messageModel from './message.model';
import userModel from './user.model';

export { messageModel, userModel };
