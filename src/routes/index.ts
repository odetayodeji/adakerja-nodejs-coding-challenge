import { WebhookRoutes } from './webhook.route';
import { MessagesRoutes } from './messages.routes.config';

export { WebhookRoutes, MessagesRoutes };
