import { CommonRoutesConfig } from '../common/common.routes.config';
import { WebhookController } from '../controllers/index';

import express from 'express';

export class WebhookRoutes extends CommonRoutesConfig {
    constructor(app: express.Application) {
        super(app, 'WebhookRoutes');
    }

    configureRoutes() {
        this.app.route('/webhook').get(WebhookController.getMessage).post(WebhookController.postMessage);

        return this.app;
    }
}
