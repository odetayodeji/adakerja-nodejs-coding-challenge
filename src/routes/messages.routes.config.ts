import { CommonRoutesConfig } from '../common/common.routes.config';
import { MessageController } from '../controllers/index';
import MessageMiddleware from '../middleware/messages.middleware';

import express from 'express';

export class MessagesRoutes extends CommonRoutesConfig {
    constructor(app: express.Application) {
        super(app, 'MessagesRoutes');
    }

    configureRoutes() {
        this.app.route('/messages').get(MessageController.getMessages);

        this.app
            .route(`/messages/:id`)
            .all(MessageMiddleware.validateMessageExists)
            .get(MessageController.getMessageById);

        return this.app;
    }
}
