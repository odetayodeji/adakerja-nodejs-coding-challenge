import express from 'express';
import { userServices } from '../services/index';
import { success, error } from '../helpers/api.response';

class UserController {
    async createUser(req: express.Request, res: express.Response) {
        try {
            const user = await userServices.createNewUser(req.body);
            if (user) {
                return res.status(201).json(success('user created successfully', { user }, res.statusCode));
            } else {
                res.status(500).json(error('Something went wrong', res.statusCode));
            }
        } catch (error) {
            res.status(500).json({ msg: error });
        }
    }

    async getUserById(req: express.Request, res: express.Response) {
        try {
            let senderId = Number(req.params.id);

            const user = await userServices.getUser(senderId);
            if (user) {
                return res.status(200).json(success('user fetched successfully', { user }, res.statusCode));
            } else {
                res.status(404).json(error('user not found', res.statusCode));
            }
        } catch (error) {
            res.status(500).json({ msg: error });
        }
    }

    async updateUserById(req: express.Request, res: express.Response) {
        try {
            let senderId = Number(req.params.id);
            const user = await userServices.updateUser(senderId, req.body);
            if (user) {
                return res.status(200).json(success('user updated successfully', { user }, res.statusCode));
            }
            res.status(500).json(error('server error', res.statusCode));
        } catch (error) {
            res.status(500).json({ msg: error });
        }
    }

    async getUsers(req: express.Request, res: express.Response) {
        try {
            const users = await userServices.getAllUsers();
            if (users) {
                return res.status(200).json(success('users fetched successfully', { users }, res.statusCode));
            } else {
                res.status(500).json(error('server error', res.statusCode));
            }
        } catch (error) {
            res.status(500).json({ msg: error });
        }
    }
}

export default new UserController();
