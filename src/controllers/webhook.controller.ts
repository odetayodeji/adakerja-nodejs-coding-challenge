import { userServices, messageServices } from '../services/index';
import express from 'express';
import request from 'request';
import dayjs from 'dayjs';

export interface IMessage {
    senderId: number;
    timestamp: number;
    mid: string;
    text: string;
}

const postMessage = async (req: express.Request, res: express.Response) => {
    const body = req.body;
    if (body.object === 'page') {
        const webhook_event = body.entry[0].messaging[0];
        console.log(webhook_event);
        await HandleMessage({
            senderId: webhook_event.sender.id,
            timestamp: webhook_event.timestamp,
            mid: webhook_event.message.mid,
            text: webhook_event.message.text
        });
        res.status(200).send('EVENT_RECEIVED_SUCCESSFULY');
    } else {
        res.sendStatus(404);
    }
};

const getMessage = async (req: express.Request, res: express.Response) => {
    const VERIFY_TOKEN = process.env.VERIFY_TOKEN;

    const mode = req.query['hub.mode'];
    const token = req.query['hub.verify_token'];
    const challenge = req.query['hub.challenge'];

    if (mode && token) {
        if (mode === 'subscribe' && token === VERIFY_TOKEN) {
            console.log('WEBHOOK_VERIFIED');
            res.status(200).send(challenge);
        } else {
            res.sendStatus(403);
        }
    }
};

const HandleMessage = ({ senderId, timestamp, mid, text }: IMessage): Promise<string> => {
    return new Promise(async (resolve, reject) => {
        await messageServices.createNewMessage({
            senderId,
            timestamp,
            mid,
            text
        });
        const user = await userServices.getUser(senderId);
        if (!user) {
            userServices.createNewUser({ senderId });
            sendMessage(senderId, "What's your name?");
        } else {
            if (!user.firstName) {
                userServices.updateUser(senderId, { firstName: text });
                sendMessage(senderId, 'When is your birth date? (please reply with this format: YYYY-MM-DD)');
            } else {
                if (!user.birthDate) {
                    if (dayjs(text, 'YYYY-MM-DD').isValid()) {
                        userServices.updateUser(senderId, { birthDate: text });
                        sendMessage(senderId, 'Do you want to know how many days until your next birhtday?');
                    } else {
                        sendMessage(
                            senderId,
                            'Please reply with this exact format for your birth date: YYYY-MM-DD (e.g. 1971-06-22)'
                        );
                    }
                } else {
                    if (['yes', 'yeah', 'y', 'yup', 'yes please'].includes(text.toLowerCase())) {
                        const nextBirthday = dayjs(user.birthDate).year(dayjs().year());
                        const daysToGo = nextBirthday.diff(dayjs(), 'days');
                        sendMessage(senderId, `${daysToGo} days to go`);
                    } else if (['no', 'nah', 'n', 'nope'].includes(text.toLowerCase())) {
                        sendMessage(senderId, 'Goodbye !');
                    } else {
                        sendMessage(senderId, 'Do you want to know how many days until your next birthday?');
                    }
                }
            }
        }

        resolve('message sent');
    });
};

const sendMessage = (senderId: number, text: string) => {
    const requestBody = {
        recipient: {
            id: senderId
        },
        message: {
            text
        }
    };

    request(
        {
            uri: 'https://graph.facebook.com/v12.0/me/messages',
            qs: {
                access_token: process.env.PAGE_ACCESS_TOKEN
            },
            method: 'POST',
            json: requestBody
        },
        (err, res, body) => {
            if (!err && res.statusCode == 200) {
                console.log('Message sent successfully!');
            } else {
                console.error('An error occurred while sending message:' + err);
            }
        }
    );
};

export default {
    HandleMessage,
    postMessage,
    getMessage
};
