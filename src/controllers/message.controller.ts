import express from 'express';
import { messageServices } from '../services/index';
import { success, error } from '../helpers/api.response';

class MessageController {
    async createMessage(req: express.Request, res: express.Response) {
        try {
            const message = await messageServices.createNewMessage(req.body);
            if (message) {
                return res.status(201).json(success('message created successfully', { message }, res.statusCode));
            } else {
                res.status(500).json(error('Something went wrong', res.statusCode));
            }
        } catch (error) {
            res.status(500).json({ msg: error });
        }
    }

    async getMessageById(req: express.Request, res: express.Response) {
        try {
            const message = await messageServices.getMessage(req.params.message_id);
            if (message) {
                return res.status(200).json(success('message fetched successfully', { message }, res.statusCode));
            }
            res.status(500).json(error('server error', res.statusCode));
        } catch (error) {
            res.status(500).json({ msg: error });
        }
    }

    async updateMessageById(req: express.Request, res: express.Response) {
        try {
            const message = await messageServices.updateMessage(req.params.message_id, req.body);
            if (message) {
                return res.status(200).json(success('message fetched successfully', { message }, res.statusCode));
            }
            res.status(500).json(error('server error', res.statusCode));
        } catch (error) {
            res.status(500).json({ msg: error });
        }
    }

    async getMessages(req: express.Request, res: express.Response) {
        try {
            const messages = await messageServices.getAllMessages();
            if (messages) {
                return res.status(200).json(success('messages fetched successfully', { messages }, res.statusCode));
            } else {
                res.status(500).json(error('server error', res.statusCode));
            }
        } catch (error) {
            res.status(500).json({ msg: error });
        }
    }
}

export default new MessageController();
