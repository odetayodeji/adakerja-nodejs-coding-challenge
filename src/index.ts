import express from 'express';
import * as http from 'http';
import * as winston from 'winston';
import * as expressWinston from 'express-winston';
import cors from 'cors';
import { CommonRoutesConfig } from './common/common.routes.config';
import { WebhookRoutes, MessagesRoutes } from './routes';
import debug from 'debug';
import dotenv from 'dotenv';
import mongoose, { ConnectOptions } from 'mongoose';
import * as rateLimit from 'express-rate-limit';
dotenv.config();

const app: express.Application = express();
const server: http.Server = http.createServer(app);
const port = process.env.PORT || 3000;
const routes: Array<CommonRoutesConfig> = [];
const debugLog: debug.IDebugger = debug('app');

const limiter = rateLimit.default({
    windowMs: 10 * 60 * 1000,
    max: 20
});

//apply to all routes
app.use(limiter);

app.use(express.json());
app.use(cors());

const loggerOptions: expressWinston.LoggerOptions = {
    transports: [new winston.transports.Console()],
    format: winston.format.combine(
        winston.format.json(),
        winston.format.prettyPrint(),
        winston.format.colorize({ all: true })
    )
};

if (!process.env.DEBUG) {
    loggerOptions.meta = false;
    if (typeof global.it === 'function') {
        loggerOptions.level = 'http';
    }
}

app.use(expressWinston.logger(loggerOptions));
routes.push(new WebhookRoutes(app));
routes.push(new MessagesRoutes(app));

const runningMessage = `Server running on port ${port}`;

mongoose.connect(
    `${process.env.DB_URI}`,
    {
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true
    } as ConnectOptions,

    () => {
        routes.forEach((route: CommonRoutesConfig) => {
            debugLog(`Routes configured for ${route.getName()}`);
        });
        console.log(`connected to ${process.env.DB_URI}`);
        server.listen(port, () => {
            console.log(`server is listening on port ${port}`);
        });
    }
);
//base endpoint
app.get('/', (req: express.Request, res: express.Response) => {
    res.status(200).send({
        'health-check': 'OK',
        message: runningMessage
    });
});
