# This is a multi stage build for production, 
# The first stage is to build everything that we need for the Node.js application to run,
# installing npm packages

# --------------> The build image
FROM node:latest AS build

WORKDIR /usr/src/app

COPY package*.json /usr/src/app/

#This will install only production dependencies and ignore DevDepencies
RUN npm ci --only=production


# This is the production image that can be optimized and published to a registry

# --------------> The production image
FROM node:lts-alpine


# For optimal performance and security, ENV is set to production
ENV NODE_ENV production

USER node

WORKDIR /usr/src/app

COPY --chown=node:node --from=build /usr/src/app/node_modules /usr/src/app/node_modules

COPY --chown=node:node . /usr/src/app

CMD [ "npm", "start"]
